from django.db.models import TextField, CharField
from django.core.exceptions import ValidationError
from . import widgets

def unallowed_tags(value):
    value = value.lower()

    if "<script" in value:
        raise ValidationError('Questo campo contiene script!')
    if "<frame" in value or "<iframe" in value:
        raise ValidationError('Questo campo contiene frame!')


class RichTextField(TextField):
    def formfield(self, **kwargs):
        kwargs['widget'] = widgets.RichTextarea
        kwargs['validators'] = kwargs.get('validators', list()) + [unallowed_tags,]

        return super(RichTextField, self).formfield(**kwargs)


class ColorField(CharField):
    def __init__(self, *args, **kwargs):
        kwargs['blank'] = True
        kwargs['max_length'] = 7
        super(ColorField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        kwargs['widget'] = widgets.ColorPicker
        return super(ColorField, self).formfield(**kwargs)
