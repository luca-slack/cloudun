from re import split

from django.conf import settings

LANGUAGES = settings.LANGUAGES
DEFAULT_LANG = settings.DEFAULT_LANG


class LanguageMiddleware(object):

    def process_template_response(self, request, response):
        lang = request.session.get('LANG', DEFAULT_LANG)

        # Update context_data (lang)
        response.context_data['lang'] = lang

        # Update context_data (canonical)
        response.context_data['canonical'] = "{}/{}{}".format( request.get_host(), lang, request.path )
        # See https://docs.djangoproject.com/en/dev/ref/request-response/#django.http.HttpRequest.get_host

        # Update template name
        tn = response.template_name
        response.template_name = [ "{}/{}".format(lang, tn) ] + tn

        # Ok
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        # Store preferred language into session

        # Language from kwargs or session
        lang = view_kwargs.get('lang', request.session.get('LANG', False))

        if not lang:
            # Accepted languages (by client)
            accepted_langs = split('[-,;]', request.META.get('HTTP_ACCEPT_LANGUAGE', DEFAULT_LANG))

            # Accepted languages (by app)
            supported_langs = [x for x in accepted_langs if x in LANGUAGES]

            # Save
            if len(supported_langs) == 0:
                request.session['LANG'] = DEFAULT_LANG
            else:
                request.session['LANG'] = supported_langs[0]
