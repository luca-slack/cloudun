from django.forms import widgets

class RichTextarea(widgets.Textarea):
    def __init__(self, attrs=None):
        default_attrs = {
            "class" : "ckeditor"
        }

        if attrs:
            default_attrs.update(attrs)

        super(RichTextarea, self).__init__(default_attrs)

    class Media:
        js = ("ckeditor/ckeditor.js", "ckeditor_admin.js", )


class ColorPicker(widgets.TextInput):
    input_type = "color"
